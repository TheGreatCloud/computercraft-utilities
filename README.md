# ComputerCraft utilities

Hopefully comments in code should be sufficient for documentation.

Because even when I play videogames, I just have to become a software developer again, to feel like myself (and instead of wasting hours on doing some magical things, I code up a solution in twice that time because then I'll know that it's indeed scalable).

I'm writing this documentation mainly for my own future self, but if you accidentally stumbled on this, welcome, and I hope that you are having a great day :)

# ComputerCraft root directory

Files in the root directory are runnable through a Turtle/Computer/Phone's interface easily, so it is important to keep this as clean as possible. Commands such as build are created for this purpose.

## Startup
The Startup.lua file is shared between all computers due to the design of the sharing/distribution Python script (`share.py`). Because of this, I differentiate between computers using their labels. This allows for easy synchronization and distribution.

## Harvest
The primary reason why this whole thing started. Turtles are normally ...

### Problems
Go home assumes that the entire plane where the turtle can go is flat, and I didn't want to create a more advanced pathfinding with such limited tools. Just box the turtle in instead.

### Logging
In-game computers don't have tools to easily display large amounts of information, so read the file written by the logger computer instead.