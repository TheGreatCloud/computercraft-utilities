# This is meant to be ran separately, and copies all computercraft files into all computers.
# Only computers that have their own IDs will receive these, which can be achieved by making, then deleting a directory: `mkdir asd; rm asd`.

import os
from subprocess import call

computercraft_folder = ' <<Minecraft server root directory>> /world/computercraft/computer'
for folder in os.listdir(computercraft_folder):
	print(call(['robocopy', f'{__file__}/../computer_root_dir/', f'{computercraft_folder}/{folder}/', '/E'], shell=True))
