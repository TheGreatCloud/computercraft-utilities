-- The script named 'startup.lua' runs on every startup!


-- Set globals
_G.inferium_harvesters_port = 101
_G.inferium_responders_port = 102


-- Start default loops
if os.computerLabel() ~= nil then 

	-- Inferium harvester mainloop
	if string.sub(os.computerLabel(), 1, 18) == 'Inferium Harvester' then
		shell.run('_harvest/turtle_mainloop.lua')
	end

	-- GPS hosts
	if string.sub(os.computerLabel(), 1, 8) == 'GPS_host' then
		if string.sub(os.computerLabel(), 10, 10) == '0' then
			shell.run('gps', 'host', -37, 40, 336)
		elseif string.sub(os.computerLabel(), 10, 10) == '1' then
			shell.run('gps', 'host', -32, 40, 336)
		elseif string.sub(os.computerLabel(), 10, 10) == '2' then
			shell.run('gps', 'host', -37, 40, 331)
		elseif string.sub(os.computerLabel(), 10, 10) == '3' then
			shell.run('gps', 'host', -37, 45, 336)
		else
			print('Unable to set up GPS hosts for '..os.computerLabel())
		end
	end

	if os.computerLabel() == 'Inferium Harvest Logger' then
		shell.run('_harvest/logger_mainloop.lua')
	end

end -- os.computerLabel() ~= nil

print('Custom startup script finished!')