-- This code was adapted from the built-in tunnel code.

-- Set names
local tArgs = { ... }
local vDirection
local hDirection
local length
local hLength
local vLength
if #tArgs == 5 then
	vDirection = tArgs[1]
	hDirection = tArgs[2]
	length = tonumber( tArgs[3] )
	hLength = tonumber ( tArgs[4] )
	vLength = tonumber ( tArgs[5] )
elseif #tArgs == 4 then
	vDirection = string.sub(tArgs[1], 1, 1)
	hDirection = string.sub(tArgs[1], 2, 2)
	print("Horizonal: "..hDirection.." | Vertical: "..vDirection)
	length = tonumber( tArgs[2] )
	hLength = tonumber ( tArgs[3] )
	vLength = tonumber ( tArgs[4] )
else
	print( "Usage: dig3 <up/down> <right/left> <length forward> <length right/left> <length up/down>" )
	return
end

local function matches(long, short) return (string.sub(long, 1, string.len(short)) == short) end
local function autocomplete(str)
	if 		matches("down", str) then return "down"
	elseif 	matches("up", str) then return "up"
	elseif 	matches("left", str) then return "left"
	elseif 	matches("right", str) then return "right"
	else print("Could not autocomplete "..str) end
end
hDirection = autocomplete(hDirection)
vDirection = autocomplete(vDirection)

-- Check for parameter correctness
if length < 1 or hLength < 1 or vLength < 1 then
	print( "Tunnel length, width and height must all be positive. They are: f"..length..", h"..hLength..", v"..vLength )
	return
end
if hDirection ~= "left" and hDirection ~= "right" then
	if hDirection == "up" or hDirection == "down" then
		hDirection, vDirection = vDirection, hDirection
	else
		print( "Horizontal direction specified must be \"left\" or \"right\". It is instead: "..hDirection )
		return
	end
end
if vDirection ~= "up" and vDirection ~= "down" then
	-- Swapping already took place in other check
	print( "Vertical direction specified must be \"up\" or \"down\". It is instead: "..vDirection )
	return
end
	
-- Define helpers
local collected = 0

local function tryDig(digging, moving, errormessage)
	-- This digs as long as something falls in front of the
	while digging() do
		-- Print information
		collected = collected + 1
		if math.fmod(collected, 25) == 0 then
			print( "Mined "..collected.." blocks." ) end
		sleep(0.5)
	end
	if moving ~= nil then 
		if not moving() then
			print( "Can't move "..errormessage..". Stopping in place..." )
			return false
		end
	end
	return true
end

local function tryDigForward(amount)
	for r=1, amount do
		if not tryDig(turtle.dig, turtle.forward, "forward") then
			return false
		end
	end
	return true
end

local function tryDigUp() return tryDig(turtle.digUp, turtle.up, "up") end

local function tryDigDown() return tryDig(turtle.digDown, turtle.down, "down") end

local function moveForwardBy(amount)
	amount = amount or 1
	for r=1,amount do
		if not turtle.forward() then
			print( "Can't move forward. Stopping in place..." )
			return false
		end
	end
	return true
end

local function turnAround()
	turtle.turnLeft()
	turtle.turnLeft()
end

local function turnSideways()
	if hDirection == "right" then return turtle.turnRight() else return turtle.turnLeft() end end

local function turnSidewaysOther()
	if hDirection == "left" then return turtle.turnRight() else return turtle.turnLeft() end end

local function digVert()
	if vDirection == 'up' then return tryDigUp() else return tryDigDown() end end

local function digVertOther()
	if vDirection == 'down' then return tryDigUp() else return tryDigDown() end end

-- end of helpers

if turtle.getFuelLevel() < 1000 then print("Fuel is "..turtle.getFuelLevel()..". Consider refueling soon.") end

-- Mine in a quarry pattern until we hit something we can't dig
print( "Starting to dig a cube that is forward "..length..", "..hDirection.." "..hLength.." and "..vLength.." "..vDirection )

for v=1,vLength do
	for d=1,length do
		turnSideways() -- Turtle original position is the corner of the rectangle
		assert(tryDigForward(hLength-1), "Failed digging forward")
		turnAround()
		assert(moveForwardBy(hLength-1), "Failed moving back")
		turnSideways()
		if d<length then 
			assert(tryDigForward(1), "Failed to dig for next line")
		end
	end
	turnAround()
	assert(moveForwardBy(length-1), "Failed moving back (2)")
	turnAround()
	if v < vLength then
		assert(digVert(), "Failed digging vertically")
	end
end

print( "Digging complete." )

--[[
print( "Returning to start..." )

-- Return to where we started
turtle.turnAround()
while depth > 0 do
	if turtle.forward() then
		depth = depth - 1
	else
		turtle.dig()
	end
end
turtle.turnRight()
turtle.turnRight()
]]

print( "Mined "..collected.." blocks total." )
if turtle.getFuelLevel() < 1000 then print("Fuel is "..turtle.getFuelLevel()..". Consider refueling soon.") end
