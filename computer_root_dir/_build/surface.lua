local tArgs = { ... }

assert(tArgs[1] == 'surface', 'Use \'build\' to call this function!')
assert(#tArgs == 4, 'Usage: surface <r|l|u|d> <length> <width>')

local direction = tArgs[2][1]
local length = tonumber(tArgs[3])
local width  = tonumber(tArgs[4])

dir1 = turtle.turnRight
dir2 = turtle.turnLeft

-- assuming direction=right for now
local function build(amount)
	for i=1, amount do
		while turtle.getItemDetail() == nil or turtle.getItemCount() < 1 do
			assert(turtle.getSelectedSlot() < 16, 'Turtle\'s inventory is empty!')
			turtle.select(turtle.getSelectedSlot() + 1)
		end
		if not turtle.back() then error('Could not go backward!') end
		if not turtle.place() then error('Could not place block!') end
	end
end

-- assert((direction == 'r') or (direction == 'l') or (direction == 'u') or (direction == 'd'), 'Usage: surface <r|l|u|d> <length> <width>')

turtle.turnLeft()
for i=1, length do
	build(width)
	if math.fmod(i, 2) == 0 then
		turtle.turnLeft()
		turtle.forward()
		turtle.turnLeft()
		turtle.back()
	else
		turtle.turnRight()
		turtle.forward()
		turtle.turnRight()
		turtle.back()
	end
end

print('Finished building a surface!')