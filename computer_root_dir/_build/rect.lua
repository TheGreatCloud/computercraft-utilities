local tArgs = { ... }

assert(tArgs[1] == 'rect', 'Use \'build\' to call this function!')
assert(#tArgs == 4, 'Usage: rect <r|l|u|d> <length> <width>')

local direction = tArgs[2][1]
local length = tonumber(tArgs[3])
local width  = tonumber(tArgs[4])

-- assuming direction=right for now
local function build(amount)
	for i=1, amount-1 do
		if not turtle.back() then error('Could not go backward!') end
		if not turtle.place() then error('Could not place block!') end
	end
end

for i=1, 2 do
	turtle.turnLeft() -- because we are going right
	build(width)
	turtle.turnLeft()
	build(length)
end

print('Finished building a rectangle!')