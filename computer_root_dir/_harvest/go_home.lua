-- Makes turtle go back to resting spot

local modem = peripheral.find("modem")

shell.run('refuel', 'all')

local function go_home()

	if os.computerLabel() == nil or string.sub(os.computerLabel(), 1, 18) ~= 'Inferium Harvester' then
		-- Don't accidentally move anything else
		return
	end

	modem.transmit(_G.inferium_responders_port, _G.inferium_harvesters_port, '[LOG] '..os.computerLabel()..' is going home!')

	-- Get two positions
	local x, y, z = gps.locate()
	for i=1, 4 do
		if turtle.forward() then
			break
		end
		turtle.turnLeft()
	end
	local x2, y2, z2 = gps.locate()
	assert((x2 ~= x) or (z2 ~= z), 'Turtle cannot move!')

	-- Determine direction
	local direction = ''
	if 		x == x2 - 1 then turtle.turnLeft()
	elseif 	x == x2 + 1 then turtle.turnRight()
	elseif	z == z2 - 1 then turtle.turnLeft(); turtle.turnLeft()
	elseif	z == z2 + 1 then 
	else error('What the fuck?')
	end

	-- Go to appropriate Z coord (north-south)
	if z2 > 331 then
		for i=1, (z2 - 331) do turtle.forward() end
		turtle.turnRight()
	elseif z2 < 331 then
		turtle.turnLeft(); turtle.turnLeft()
		for i=1, (331 - z2) do turtle.forward() end
		turtle.turnLeft()
	end

	-- now turtle (should) face east

	-- Go to appropriate X coord (east-west)
	if x2 > -25 then
		turtle.turnLeft(); turtle.turnLeft()
		for i=1, (x2 - (-25)) do turtle.forward() end
		turtle.turnLeft()
	elseif x2 < -25 then
		for i=1, ((-25) - x2) do turtle.forward() end
		turtle.turnRight()
	else
		turtle.turnRight();
	end

	x, y, z = gps.locate()
	-- Fix one particular problem temporarily
	if x == -25 and z == 329 then
		turtle.turnLeft()
		turtle.forward()
		turtle.turnRight()
		turtle.forward()
		turtle.turnRight()
		turtle.forward()
		turtle.turnLeft()
		turtle.turnLeft()
	end

	assert(x == -25 and z == 331, 'Did not arrive properly!')

	print('Homecoming completed!')
	modem.transmit(_G.inferium_responders_port, _G.inferium_harvesters_port, '[SUCCESS] '..os.computerLabel()..' is home!')
end

if not pcall(go_home) then
	modem.transmit(_G.inferium_responders_port, _G.inferium_harvesters_port, '[ERROR] '..os.computerLabel()..' failed going home!')
end