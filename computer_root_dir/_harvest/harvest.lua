-- Harvest.lua has been forked from dig3.lua on 2023-01-24

local modem = peripheral.find("modem")

shell.run('refuel', 'all')

local function log(type, msg)
	print(msg)
	modem.transmit(_G.inferium_harvesters_port, _G.inferium_responders_port, '['..type..'] '..os.computerLabel()..' failed going home!')
end

local tArgs = { ... }
local function harvest()

	assert(#tArgs == 3, "Usage: harvest <right/left> <length forward> <length right/left>" )

	local vDirection = "up"
	local hDirection
	local length
	local hLength
	local vLength = 1
	hDirection = string.sub(tArgs[1], 1, 1)
	length = tonumber( tArgs[2] )
	hLength = tonumber ( tArgs[3] ) + 1

	local function matches(long, short) return (string.sub(long, 1, string.len(short)) == short) end
	local function autocomplete(str)
		if 		matches("down", str) then return "down"
		elseif 	matches("up", str) then return "up"
		elseif 	matches("left", str) then return "left"
		elseif 	matches("right", str) then return "right"
		else print("Could not autocomplete "..str) end
	end
	hDirection = autocomplete(hDirection)

	-- Check for parameter correctness
	if length < 1 or hLength < 1 then
		print( "Tunnel length, width and height must all be positive. They are: f"..length..", h"..hLength )
		return
	end
	if hDirection ~= "left" and hDirection ~= "right" then
		if hDirection == "up" or hDirection == "down" then
			hDirection, vDirection = vDirection, hDirection
		else
			print( "Horizontal direction specified must be \"left\" or \"right\". It is instead: "..hDirection )
			return
		end
	end
		
	-- Don't even start if conditions are not met and set default state:

	if turtle.getFuelLevel() < (hLength * length * 2) + 20 then
		log('LOG', 'Not enough fuel!')
		return
	end
	turtle.select(1)
	local desiredItemName = 'mysticalagriculture:inferium_seeds'
	if turtle.getItemDetail() == nil or turtle.getItemDetail()['name'] ~= desiredItemName then
		print('ERROR: Selected item in slot 1 is not '..desiredItemName..'!')
		print('Aborting...')
		return
	end

	-- Define helpers
	local collected = 0

	local function tryDig(digging, moving, errormessage)
		-- This digs as long as something falls in front of the
		while digging() do
			-- Print information
			collected = collected + 1
			if math.fmod(collected, 25) == 0 then
				print( "Mined "..collected.." blocks." ) end
			sleep(0.5)
		end
		if moving ~= nil then 
			if not moving() then
				print( "Can't move "..errormessage..". Stopping in place..." )
				return false
			end
		end
		return true
	end

	local function harvestInferium()
		success, data = turtle.inspectDown()
		if success and data['state']['age'] == 7 then
		  	assert(turtle.digDown(), 'Could not dig block below turtle')
		  	assert(turtle.getItemDetail() ~= nil and turtle.getItemDetail()['name'] == 'mysticalagriculture:inferium_seeds',
		  		'ERROR: When placing seed back, seed is not selected or there is no seed in inventory')
		  	assert(turtle.placeDown(), "Could not place seeds below turtle")
			return true
		end
		if not success then
			-- Try to place seed
			local prevselected = turtle.getSelectedSlot()
			for i=1, 16 do
				turtle.select(i)
				if turtle.getItemDetail() ~= nil and turtle.getItemDetail()['name'] == 'mysticalagriculture:inferium_seeds' then
					turtle.placeDown()
					break
				end
			end
			turtle.select(prevselected)
		end
		return false
	end

	local function tryDigForward(amount)
		for r=1, amount do
			if not tryDig(harvestInferium, turtle.forward, "forward") then
				return false
			end
		end
		return true
	end

	local function tryDigUp() return tryDig(turtle.digUp, turtle.up, "up") end

	local function tryDigDown() return tryDig(turtle.digDown, turtle.down, "down") end

	local function moveForwardBy(amount)
		amount = amount or 1
		for r=1,amount do
			if not turtle.forward() then
				print( "Can't move forward. Stopping in place..." )
				return false
			end
		end
		return true
	end

	local function turnAround()
		turtle.turnLeft()
		turtle.turnLeft()
	end

	local function turnSideways()
		if hDirection == "right" then return turtle.turnRight() else return turtle.turnLeft() end end

	local function turnSidewaysOther()
		if hDirection == "left" then return turtle.turnRight() else return turtle.turnLeft() end end

	local function digVert()
		if vDirection == 'up' then return tryDigUp() else return tryDigDown() end end

	local function digVertOther()
		if vDirection == 'down' then return tryDigUp() else return tryDigDown() end end

	-- end of helpers

	if turtle.getFuelLevel() < 1000 then print("Fuel is "..turtle.getFuelLevel()..". Consider refueling soon.") end

	-- Mine in a quarry pattern until we hit something we can't dig
	print( "Starting to harvest in an area that is forward "..length..", and "..hDirection.." "..hLength)

	assert(moveForwardBy(1), 'Failed moving forward by one!')
	for d=1,length do
		turnSideways() -- Turtle original position is the corner of the rectangle
		assert(tryDigForward(hLength-1), "Failed digging forward")
		turnAround()
		assert(moveForwardBy(hLength-1), "Failed moving back to start next column!")
		turnSideways()
		if d<length then 
			assert(tryDigForward(1), "Failed to dig for next line")
		end
	end
	turnAround()
	assert(moveForwardBy(length), "Failed moving back to starting position!")
	turnAround()

	print( "Harvesting complete." )

	log('LOG', "harvested "..collected.." blocks total." )
	if turtle.getFuelLevel() < 1000 then print("Fuel is "..turtle.getFuelLevel()..". Consider refueling soon.") end

	-- Attempt to deposit harvested stuff into chest
	success, data = turtle.inspectDown()
	if not success then
		print('No block found below turtle while trying to deposit harvested items!')
	elseif data['name'] ~= 'minecraft:chest' then
		print('ERROR: The block found below the turtle expected to be a chest, but is in fact a '..data['name']..'! Aborting deposition.')
	else
		-- Place harvested inferium in chest
		for slot=2, 16 do
			turtle.select(slot)

			if turtle.getItemDetail() ~= nil and (
					turtle.getItemDetail()['name'] == 'mysticalagriculture:inferium_essence' or
					turtle.getItemDetail()['name'] == 'mysticalagriculture:inferium_seeds') then
				-- Lua has no 'continue' keyword, so it has to be inverted
				if not turtle.dropDown() then
					print('Inventory is full, aborting offloading.')
					break
				end
			end
		end
	end

	print('Returning to default state.')
	turtle.select(1)
	local desiredItemName = 'mysticalagriculture:inferium_seeds'
	if turtle.getItemDetail()['name'] ~= desiredItemName then
		log('WARNING', 'Selected item is not '..desiredItemName..'!')
	end

end

if not pcall(harvest) then
	modem.transmit(_G.inferium_responders_port, _G.inferium_harvesters_port, '[ERROR] '..os.computerLabel()..' failed harvesting!')
end