-- Sends stuff to receiver

local modem = peripheral.find("modem")
assert(os.computerLabel() ~= nil and #os.computerLabel() >= 20)
modem.transmit(_G.inferium_responders_port, _G.inferium_harvesters_port, '[INFO] Turtle '..string.sub(os.computerLabel(), 20, 999)..' Fuel: '..turtle.getFuelLevel())