print('Turtle main loop started at '..textutils.formatTime(os.time("local"), true)..'!')

shell.run('_harvest/go_home.lua')

_G.modem = peripheral.find("modem")
modem.open(_G.inferium_harvesters_port)

while true do
	print('Attempting to receive message on channel '.._G.inferium_harvesters_port..'...')
	local event, peripheral_name, channel, replyChannel, message, distance = os.pullEvent("modem_message")

	if (message == 'go home') or (message == 'gohome') then
		shell.run('_harvest/go_home.lua')

	elseif message == 'harvest' then
		shell.run('_harvest/harvest.lua', 'right', 18, 18)
		shell.run('_harvest/go_home.lua')

	elseif (message == 'restart all') or (message == 'restartall') then
		os.reboot()

	elseif message == 'info' then
		shell.run('_harvest/report_status.lua')

	else
		print('Unhandled message received: ')
		print(message)

	end
end