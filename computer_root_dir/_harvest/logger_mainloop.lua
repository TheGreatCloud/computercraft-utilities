print('Inferium harvester main loop started at '..textutils.formatTime(os.time("local"), true)..'!')

local modem = peripheral.find("modem")
modem.open(_G.inferium_responders_port)

logfile = fs.open('_harvest/logs.txt', 'a')

while true do
	local event, peripheral_name, channel, replyChannel, message, distance = os.pullEvent("modem_message")
	if string.sub(message, 1, 5) == '[ERROR]' then
		print(message)
	end
	logfile.write(textutils.formatTime(os.time("local"), true)..' '..message..'\n')
	logfile.flush()
end