local documentation_str = 'Usage: send <what>'
assert(select('#', ...) == 1, documentation_str)

modem = peripheral.find('modem') or error('Cant find modem!')
modem.transmit(_G.inferium_harvesters_port, _G.inferium_responders_port, select(1, ...))